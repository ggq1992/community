## SIG简介

MindQuantum致力于和开发者共同打造灵活高效、易学易用的量子计算框架，持续提升MindQuantum功能、性能和易用性，助力量子计算技术研究和创新，主要工作包括：

1. 生态推广：拓展开发者使用MindQuantum进行学习和研究。
2. 需求洞察：通过对量子计算发展趋势洞察和技术研究诉求，识别MindQuantum关键需求。
3. 版本规划：基于研究创新和价值分析，对需求内容和应用场景等进行分析细化，落实到版本。
4. 特性开发：基于版本规划，完成新功能开发和测试。
5. 版本适配：随着MindQuantum版本迭代，对已支持的特性进行更新和适配。
6. 教程开发和优化：开发和优化MindQuantum教程，为其制作中英文图文，视频教程，帮助开发者更方便的了解和使用MindQuantum。
7. 互动答疑：在社区/微信群/论坛等为开发者答疑解惑，活跃社区，提升开发者满意度。

MindQuantum SIG是为广大开发者提供的共同交流和学习的平台，欢迎大家一起来分享、交流和共同提升MindQuantum功能特性，帮助开发者学好，用好MindQuantum。SIG成员将获得更多专家指导和开发资源支持。

## MindQuantum 代码仓

1. [MindQuantum 代码仓](https://gitee.com/mindspore/mindquantum)
2. [MindQuantum SIG工作目录](https://gitee.com/mindspore/community/tree/master/sigs/mindquantum)

## Maintainers

* dorothy（MindQuantum社区运营接口人，负责社区运营、宣传推广、优秀开发者和布道师发展等）
* 周旭（MindQuantum社区资深开发者、布道师，负责MindQuantum生态推广、特性开发和需求收集等）

## Contributors

* donghufeng（MindQuantum社区资深开发者、布道师，负责MindQuantum核心模块开发）
* 谢晴兴（MindQuantum社区资深开发者，负责MindQuantum生态推广、特性开发和需求收集等）
* 何天深（MndQuantum社区资深开发者，负责MindQuantum生态推广、特性开发和需求收集等）
* 何润洪（MindQuantum社区优秀开发者，负责MindQuantum生态推广、特性开发和需求收集等）
* 唐加亮（MindQuantum社区优秀开发者，负责MindQuantum生态推广、特性开发和需求收集等）
* 储贻达（MindQuantum社区优秀贻开发者，负责MindQuantum生态推广、特性开发和需求收集等）
* 阿拉帕提·阿不力米提（MindQuantum社区优秀开发者，负责MindQuantum生态推广、特性开发和需求收集等）
* 陈泓霖（MindQuantum社区优秀开发者，负责MindQuantum生态推广、特性开发和需求收集等）
* 闫琪（MindQuantum社区信息体验专家，负责MindQuantum资料与产品体验系列活动组织、用户满意度调查等）

## 2022年目标

1. 组织管理：制定SIG组织管理规范，邀请Maintainers和Contributors初始成员参与SIG运作（至少2名Maintainers和10名Contributors）。
2. 生态拓展：发展30名SIG组正式成员；发展3名布道师、5位优秀开发者。
3. 特性开发：收集10+关键特性，规划5+新特性，与开发者共同完成特性开发。
4. 活动开展：MindQuantum开源成长计划活动参与人次100+，收集改进建议50+；开展技术分享直播10+次，发表技术分享10+篇；拓展高校10+所/300人。

## 主要活动

### 1. 线上技术分享会

* 活动定位：分享MindQuantum技术和特性，促进开发者深入使用MindQuantum。
* 活动形式：定期邀请业界专家、高校师生、资深开发者分享话题。
* 活动频度：每1-2个月/次，每次围绕同一个主题分享3-4个话题。
* 分享范围：
  1. MindQuantum常用特性/新特性介绍与演示。
  2. MindQuantum特性改进建议、特性需求收集、使用体验反馈。
  3. MindQuantum学习心得、开发经验、使用建议。
  4. MindQuantum开发任务分发、设计评议、开发讨论、验收颁奖等。
* 分享嘉宾：任何SIG成员包括高校师生、业界专家、开发者均可。
* 组织者：Maintains和Contributors。

### 2. MindQuantum特性开发

* 活动定位：共同参与MindQuantum特性开发，丰富功能，提升性能。
* 活动形式：定期发布大颗粒特性开发任务，招募开发者共同参与，完成者可获得奖品或者实习工资、实习证明。
* 活动频度：每季度发布/刷新任务。
* 任务发布：
  1. [MindQuantum 开源实习](https://gitee.com/mindspore/community/issues/I55XLC?from=project-issue)。
  2. [社区常规任务](https://gitee.com/mindspore/community/issues/I4YQNG?from=project-issue)
* 组织者：Maintains和Contributors。

### 3. 资料与产品体验改进活动

* 活动定位：降低MindQuantum入门门槛，使开发者快速上手，由浅入深了解MindQuantum的能力。收集建议及体验评价，持续提升资料与产品综合体验。
* 活动形式：开发者通过提交Issue/PR形式提出问题建议或修改，积累积分获得奖品。
* 活动频度：不定期举行。
* 活动规划：
  1. 资料体验：系列活动，针对上线的教程、API、视频等内容，开展众测体验活动。
  2. 产品体验：随新发布版本进行，针对版本发布的功能特性开展体验活动。
* 组织者：Maintains和Contributors轮流组织。

### 4. 周例会

* 时间：周一晚上7点，每1-2周开展一次。
* 例会内容：面向SIG特性开发和组织管理工作，进行开放式的例行交流。
* 例会议题：
  1. 固定议题：SIG组成员领取的特性开发任务进展与问题交流。
  2. 选报议题：特性开发阶段性成果演示。
  3. 选报议题：SIG组织管理（如运作规则，成员及其职责等刷新）。
* 组织者：Maintains和Contributors轮流组织。

## 近期活动与例会预告

1. 技术分享会（量子计算组会一起开，寇享直播）
2. SIG组例会

## 往期活动与例会预告